<?php
/**
 * @file
 * Theme override and preprocess functions for Get Skeleton.
 */

/**
 * Implements hook_preprocess_html().
 */
function responsive_skeleton_preprocess_html(&$vars) {
  $meta = array(
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
    ),
  );
  drupal_add_html_head($meta, 'responsive_skeleton-viewport');
  drupal_add_js('http://html5shim.googlecode.com/svn/trunk/html5.js');

  if (!isset($vars['rdf']) || !is_object($vars['rdf'])) {
    $vars['rdf'] = new StdClass();
  }

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  }
  else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
}

/**
 * Implements hook_preprocess_page().
 */
function responsive_skeleton_preprocess_page(&$variables) {
  switch ($variables['layout']) {
    case 'first':
      $variables['left_classes'] = ' three columns';
      $variables['content_classes']  = ' thirteen columns';
      break;

    case 'second':
      $variables['content_classes']  = ' thirteen columns';
      $variables['right_classes'] = ' three columns';
      break;

    case 'both':
      $variables['left_classes'] = ' three columns';
      $variables['content_classes'] = ' ten columns';
      $variables['right_classes'] = ' three columns';
      break;

    case 'none':
      $variables['content_classes'] = ' sixteen columns';
      break;

    case 'default':
      $variables['content_classes'] = ' sixteen columns';
  }
}
